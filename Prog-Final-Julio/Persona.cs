﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Final_Julio
{
    public class Persona
    {
        public int DNI { get; set; }
        public string NombreYApellido { get; set; }
        public int CodigoPostal { get; set; }
        public int TelContacto { get; set; }
    }
}
