﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Prog_Final_Julio.Envio;
using static Prog_Final_Julio.EventosAsociados;

namespace Prog_Final_Julio
{
    public class Principal
    {
        public List<Envio> Envios = new List<Envio>();
        public List<Encargado> Repartidores = new List<Encargado>();
        public List<Destinatario> Destinatarios = new List<Destinatario>();


        public int CargarNuevoEnvio(int dniDestinatario, DateTime fechaEntregaEstimada, string descripcionPaquete)
        {
            Envio nuevoEnvio = new Envio();
            EventosAsociados nuevoEvento = new EventosAsociados();
            nuevoEnvio.EnvioAutoNumerico = nuevoEnvio.GenerarCodigoAutoincremental();
            nuevoEnvio.DNIDesitnatario = dniDestinatario;
            nuevoEnvio.FechaEstimadaEntrega = fechaEntregaEstimada;
            nuevoEnvio.EnvioProgramado = false;
            nuevoEnvio.DNIRepartidor = 0;
            nuevoEnvio.EstadoEnvio = tipo.PENDIENTE_ENVIO;
            nuevoEvento.FechaEvento = DateTime.Now;
            nuevoEvento.TipoEvento = EventosAsociados.tipo2.LLEGADA_AL_CENTRO_DE_DISTRIBUCION;
            nuevoEnvio.EventosAsociados.Add(nuevoEvento);
            Envios.Add(nuevoEnvio);
            return nuevoEnvio.EnvioAutoNumerico;
        }

        public bool ActualizarEnvio(int nroEnvio,tipo2 eventoAsociado) 
        {
            Envio envioencontrado = Envios.Find(x => x.EnvioAutoNumerico == nroEnvio);
            if (envioencontrado!= null)
            {
                EventosAsociados eventos = new EventosAsociados();
                if (eventoAsociado == tipo2.EN_VIAJE)
                {
                    if (envioencontrado.EventosAsociados.Find(x=>x.TipoEvento == tipo2.LLEGADA_AL_CENTRO_DE_DISTRIBUCION) != null)
                    {
                        eventos.TipoEvento = tipo2.EN_VIAJE;
                        eventos.FechaEvento = DateTime.Now;
                        envioencontrado.EstadoEnvio = tipo.ENVIADO;
                        envioencontrado.EventosAsociados.Add(eventos);
                        return true;
                    }
                    return false;

                }
                else if (eventoAsociado == tipo2.EN_MANOS_DEL_REPARTIDOR)
                {
                    if (envioencontrado.EventosAsociados.Find(x => x.TipoEvento == tipo2.EN_VIAJE) != null)
                    {
                        eventos.TipoEvento = tipo2.EN_MANOS_DEL_REPARTIDOR;
                        eventos.FechaEvento = DateTime.Now;
                        envioencontrado.EstadoEnvio = tipo.ULTIMO_TRAMO_DEL_RECORRIDO;
                        envioencontrado.EventosAsociados.Add(eventos);
                        return true;
                    }
                    return false;
                }
                else
                {
                    if (envioencontrado.EventosAsociados.Find(x => x.TipoEvento == tipo2.EN_MANOS_DEL_REPARTIDOR) != null)
                    {
                        eventos.TipoEvento = tipo2.ENTREGADO;
                        eventos.FechaEvento = DateTime.Now;
                        envioencontrado.FechaEntrega = DateTime.Now;
                        envioencontrado.EstadoEnvio = tipo.ENTREGADO;
                        envioencontrado.EventosAsociados.Add(eventos);
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        public void AsignarRepartidor() 
        {
            foreach (var envio in Envios)
            {
                Destinatario destinatario = Destinatarios.Find(x => x.DNI == envio.DNIDesitnatario);
                foreach (var repartidor in Repartidores)
                {
                    if (repartidor.LocalidadesPermitidas.Find(x=>x == destinatario.Localidad)!= null)
                    {
                        if (repartidor.ListadoCodigoPostales.Find(x=> x == destinatario.CodigoPostal) != null)
                        {
                            
                        }
                    }
                }
            }
        }


    }
}
