﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Final_Julio
{
    public class EventosAsociados
    {
        public enum tipo2
        {
            LLEGADA_AL_CENTRO_DE_DISTRIBUCION,
            EN_VIAJE,
            EN_MANOS_DEL_REPARTIDOR,
            ENTREGADO
        }
        public tipo2 TipoEvento { get; set; }
        public DateTime FechaEvento { get; set; }
    }
}
