﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Final_Julio
{
    public class Destinatario: Persona
    {
        public string Localidad { get; set; }
        public string Direccion { get; set; }
    }
}
